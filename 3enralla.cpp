#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

#define DIM 3
#define MAX 9

// Función para pintar la cabecera
void
cabecera(){
	system ("clear");
	system ("toilet -fpagga --metal TRES EN RAYA");
	printf ("\n\n");
}

// Función que pinta el tablero de juego con las fichas
void
colocar_fichas(char tablero[DIM][DIM], unsigned fila, unsigned columna, char ficha){
	cabecera();
	tablero[fila][columna]=ficha;
}

// Función para poner el tablero vacio
void
tablero_inicial(char tablero[DIM][DIM]){
for (int x=0; x<DIM; x++)
	for (int y=0; y<DIM; y++)
		tablero[x][y]='-';
}

// Función para calcular el jugador y la ficha que necesita
void
calcular_jugador(unsigned turno, unsigned *jugador, char *ficha){
	*jugador = turno%2;
	if (*jugador == 0){
		*jugador = *jugador+2;
		*ficha = 'o';
	}
	else
		*ficha = 'x';
}

// Función para preguntar cordenadas
void
preguntar_cordenadas(unsigned jugador, char ficha, unsigned *fila, unsigned *columna){
printf ("Jugador %u, donde quieres colocar tu %c? ",jugador, ficha);
__fpurge(stdin);
scanf (" %u, %u", fila, columna);
if (*columna>DIM || *fila>DIM || *columna<0 || *fila<0)
    preguntar_cordenadas(jugador, ficha, fila, columna);
}

// Función para pintar el tablero inicial
void
pintar_tablero(char tablero[DIM][DIM]){
for (int x=0; x<DIM; x++){
	for (int y=0; y<DIM; y++)
		printf ("%c",tablero[x][y]);
	printf ("\n");
	}
	printf ("\n\n");
}

// Función para comprobar si las cordenadas apuntan a un lugar ocupado
bool
comprobar_ocupado(char tablero[DIM][DIM], unsigned fila, unsigned columna){
    if (tablero[fila][columna]=='-')
        return 1;
    else
    {
        printf ("Esas cordenadas apuntan a un lugar ocupado\n");
        return 0;
    }
}

// Función para comprobar si algún jugador ha hecho un tres en raya
bool
comprobar_final(char tablero[DIM][DIM], char ficha, unsigned jugador){
    if (tablero[0][0]==ficha && tablero[1][1]==ficha && tablero[2][2]==ficha)
    {
        printf ("Felicidades al jugador %i, ha ganado la ficha %c\n", jugador, ficha);
        return 1;
    }

    else if (tablero[0][2]==ficha && tablero[1][1]==ficha && tablero[2][0]==ficha)
    {
        printf ("Felicidades al jugador %i, ha ganado la ficha %c\n", jugador, ficha);
        return 1;
    }

    else if (tablero[0][0]==ficha && tablero[0][1]==ficha && tablero[0][2]==ficha)
    {
        printf ("Felicidades al jugador %i, ha ganado la ficha %c\n", jugador, ficha);
        return 1;
    }

    else if (tablero[1][0]==ficha && tablero[1][1]==ficha && tablero[1][2]==ficha)
    {
        printf ("Felicidades al jugador %i, ha ganado la ficha %c\n", jugador, ficha);
        return 1;
    }

    else if (tablero[2][0]==ficha && tablero[2][1]==ficha && tablero[2][2]==ficha)
    {
        printf ("Felicidades al jugador %i, ha ganado la ficha %c\n", jugador, ficha);
        return 1;
    }

    else if (tablero[0][0]==ficha && tablero[1][0]==ficha && tablero[2][0]==ficha)
    {
        printf ("Felicidades al jugador %i, ha ganado la ficha %c\n", jugador, ficha);
        return 1;
    }

    else if (tablero[0][1]==ficha && tablero[1][1]==ficha && tablero[2][1]==ficha)
    {
        printf ("Felicidades al jugador %i, ha ganado la ficha %c\n", jugador, ficha);
        return 1;
    }

    else if (tablero[0][2]==ficha && tablero[1][2]==ficha && tablero[2][2]==ficha)
    {
        printf ("Felicidades al jugador %i, ha ganado la ficha %c\n", jugador, ficha);
        return 1;
    }
    else
        return 0;
}

// MAIN
int
main (int argc, char *argv[])
{
	// Declaración de variables
	char tablero[DIM][DIM];
	char ficha = 'x';
        unsigned turno = 0;
        unsigned jugador = 1;
        unsigned fila;
        unsigned columna;

        //Ejecución
        cabecera();
        tablero_inicial(tablero);
        while (!comprobar_final(tablero, ficha, jugador) && turno < MAX){
              pintar_tablero(tablero);
              turno++;
              calcular_jugador(turno, &jugador, &ficha);
              preguntar_cordenadas(jugador, ficha, &fila, &columna);
              fila--;
              columna--;
              while(!comprobar_ocupado(tablero, fila, columna)){
                  preguntar_cordenadas(jugador, ficha, &fila, &columna);
                  fila--;
                  columna--;
              }
              colocar_fichas(tablero, fila, columna, ficha);
        }

        return EXIT_SUCCESS;
}
